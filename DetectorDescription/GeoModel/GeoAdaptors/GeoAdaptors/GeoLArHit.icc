/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "CaloDetDescr/CaloDetDescrManager.h"
#include "CaloDetDescr/CaloDetDescrElement.h"
#include "LArSimEvent/LArHitContainer.h"
#include "CaloIdentifier/CaloCell_ID.h"

inline GeoLArHit::GeoLArHit(const LArHit & h) 
  : m_hit (h)
{
}

inline const CaloDetDescrElement* GeoLArHit::getDetDescrElement() const
{
  return mgr()->get_element(m_hit.cellID());
}

inline double GeoLArHit::Energy() const
{
  return m_hit.energy();
}

inline double GeoLArHit::Time() const
{
  return m_hit.time();
}

inline int GeoLArHit::SamplingLayer() const {
  const CaloCell_ID *caloID = mgr()->getCaloCell_ID();
  int layer = caloID->sampling(m_hit.cellID());
  return layer;
}
